import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home';
import { UserListComponent } from './users';
import { ModeratorListComponent } from './moderators';
import { RecordListComponent } from './records';
import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'users',  component: UserListComponent },
  { path: 'moderators',  component: ModeratorListComponent },
  { path: 'records',  component: RecordListComponent }
];

<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\User;
use \App\Role;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;
use Validator;

class UserController extends Controller
{
  protected function getNum($role){
    $num = User::whereHas('roles', function($q) use ($role){
        $q->where('name', '=', $role);
    })->count();
    return $num;
  }

  protected function getUserNum(){
    return $this->getNum('user');
  }

  protected function getModeratorNum(){
    return $this->getNum('moderator');
  }

  protected function get(Request $request, $role){
    $page = intval($request->input('page', 1));
    $perPage = intval($request->input('per_page', 0));
    $query = User::whereHas('roles', function($q) use ($role){
        $q->where('name', '=', $role);
    });
    if($page > 0 && $perPage > 0) {
      $query = $query->take($perPage)->skip(($page - 1) * $perPage);
    }
    $users = $query->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->get();
    return $users;
  }

  public function getUsers(Request $request){
    $users = $this->get($request, 'user');
    $total = $this->getUserNum();
    return compact('users', 'total');
  }

  public function getModerators(Request $request){
    $moderators = $this->get($request, 'moderator');
    $total = $this->getModeratorNum();
    return compact('moderators', 'total');
  }

  protected function getOne($id, $role){
    $users = User::whereHas('roles', function($q) use ($role){
        $q->where('name', '=', $role);
    })->find($id);
    return $users;
  }

  public function getUser($id){
    return $this->getOne($id, 'user');
  }

  public function getModerator($id){
    return $this->getOne($id, 'moderator');
  }

  protected function populateUser(User $user, Request $request, $update){
    $validator = Validator::make($request->all(), [
      'first_name' => 'bail|required|max:255',
      'last_name' => 'bail|required|max:255',
      'password' => 'bail|required|max:255',
      'address' => 'bail|required|max:255',
      'city' => 'bail|required|max:255',
      'zip' => 'bail|required|max:255',
      'country' => 'bail|required|max:255',
      'email' => 'bail|required|email|max:255' . ($update ? '' : '|unique:users'),
      'phone' => 'bail|required|max:255',
      'note' => 'max:255',
    ]);

    if ($validator->fails()) {
      foreach($validator->errors()->all() as $error){
        throw new Exception($error);
      }
    }

    $user->first_name = $request->input('first_name');
    $user->last_name = $request->input('last_name');
    $user->password = bcrypt($request->input('password'));
    $user->address = $request->input('address');
    $user->city = $request->input('city');
    $user->zip = $request->input('zip');
    $user->country = $request->input('country');
    $user->email = $request->input('email');
    $user->phone = $request->input('phone');
    $user->note = $request->input('note');

    return $user;
  }

  protected function insert(Request $request, $role){
    $user = new User;

    try{
      $this->populateUser($user, $request, false);

      $userRole = Role::where('name','=',$role)->get()->first();

      $user->save();

      $user->attachRole($userRole);
    }
    catch (QueryException $e) {
      return response(['error' => 'user already exists'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    catch (Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function insertUser(Request $request){
    return $this->insert($request, 'user');
  }

  public function insertModerator(Request $request){
    return $this->insert($request, 'moderator');
  }

  protected function update($id, Request $request, $role){
    $user = $this->getOne($id, $role);

    if(!$user){
      return response(['error' => 'user doesn\'t exists'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    if(empty($request->input('password'))){
      $request->request->add(['password' => $user->password]);
    }

    try{
      $this->populateUser($user, $request, true);
      $user->save();
    }
    catch (Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function updateUser($id, Request $request){
    return $this->update($id, $request, 'user');
  }

  public function updateModerator($id, Request $request){
    return $this->update($id, $request, 'moderator');
  }

  protected function delete($id, $role){
    $user = $this->getOne($id, $role);

    if(!$user){
      return response(['error' => 'user doesn\'t exists'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    try{
      $user->delete();
    }
    catch (Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function deleteUser($id){
    return $this->delete($id, 'user');
  }

  public function deleteModerator($id){
    return $this->delete($id, 'moderator');
  }

  public function login(Request $request){
    $credentials = $request->only('email', 'password');

    if (!$token = JWTAuth::attempt($credentials)) {
       return response(['error' => 'invalid credentials'], Response::HTTP_UNAUTHORIZED);
    }
    $id = Auth::user()->id;
    $role = User::with('roles')
      ->find($id)
      ->roles[0]
      ->name;

    return compact('token', 'role', 'id');
  }
}

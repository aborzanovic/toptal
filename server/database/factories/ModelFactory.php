<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password = null;
    static $index = 0;

    static $names = array(
      "Jack", "Jane", "Joe", "Mike", "Edward", "Josh", "Mark", "Keith"
    );
    static $last_names = array(
      "Doe", "Malkovich", "South", "Goodman", "Goldman", "Tesla", "Greenberg"
    );
    static $addresses = array(
      "First Street", "Second Street", "Third Street", "Fourth Street",
    );
    static $cities = array(
      "London", "Paris", "New York", "Belgrade",
    );
    static $zip = array(
      "1185", "0541", "16403", "11000", "16840"
    );
    static $countries = array(
      "England", "France", "USA", "Serbia",
    );
    static $phones = array(
      "03416841", "1035102534", "13510214768", "0354643101"
    );

    $fn = $names[array_rand($names)];
    $ln = $last_names[array_rand($last_names)];
    $index++;
    return [
      'first_name' => $fn,
      'last_name' => $ln,
      'password' => $password ?: $password = bcrypt('password'),
      'address' => $addresses[array_rand($addresses)],
      'city' => $cities[array_rand($cities)],
      'zip' => $zip[array_rand($zip)],
      'country' => $countries[array_rand($countries)],
      'email' => "$fn.$ln.$index@gmail.com",
      'phone' => $phones[array_rand($phones)],
      'note' => '',
      'avatar' => '',
      //'api_token' => str_random(60)
    ];
});

$factory->define(App\Record::class, function (Faker\Generator $faker) {
    $distance = rand(5, 3000);
    $speed_factor = rand(139, 417) / 100;
    return [
        'date' => \Carbon\Carbon::createFromDate(rand(2012, 2016),rand(1, 12),rand(1, 28))
          ->toDateString(),
        'distance' => $distance,
        'time' => intval($distance * $speed_factor),
    ];
});

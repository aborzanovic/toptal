import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { RecordService } from '../services/record.service';
import { Record } from '../models/record.model';
import { MdSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'record-list',
  styleUrls: [ './record-list.component.scss' ],
  templateUrl: './record-list.component.html',
  providers: [ RecordService, MdSnackBar ]
})
export class RecordListComponent implements OnInit {
  public records: Record[] = [];
  public record: Record;
  private page: number = 1;
  private perPage: number = 5;
  private recordNum: number = 0;
  private userId: number;
  private showInsertForm: boolean = false;
  private showUpdateForm: boolean = false;
  private updateRecord: Record;

  constructor(
    public appState: AppState,
    public recordService: RecordService,
    public mdSnackBar: MdSnackBar,
    public route: ActivatedRoute
  ) {
    this.record = new Record(0,0,0,0,new Date());
    this.updateRecord = new Record(0,0,0,0,new Date());
  }

  reloadList(): void{
    this.recordService.getRecordsNum(this.userId)
    .subscribe(
        (num: number) => this.recordNum = num,
        err => {
          this.mdSnackBar.open(err, '', { duration: 1000 });
        }
    );
    this.recordService.getRecords(this.userId, this.page, this.perPage)
    .subscribe(
      (records: Record[]) => this.records = records,
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  public ngOnInit() {
    this.route.params.subscribe(params => {
       this.userId = +params['userId'];
       this.reloadList();
    });
  }

  prev(): void {
    if(this.page > 1){
      this.page -= 1;
      this.reloadList();
    }
  }

  next(): void {
    let maxPages = Math.floor(this.recordNum / this.perPage);
    if(this.recordNum % this.perPage > 0) maxPages++;
    if(this.page < maxPages){
      this.page += 1;
      this.reloadList();
    }
  }

  new(obj: any): void {
    var record: Record = obj.record;
    record.userId = this.userId;
    this.recordService.insertRecord(record)
    .subscribe(
      succ => this.reloadList(),
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  update(obj: any): void {
    var record: Record = obj.record;
    record.userId = this.userId;
    this.recordService.updateRecord(record)
    .subscribe(
      succ => this.reloadList(),
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  delete(record: Record): void {
    record.userId = this.userId;
    this.recordService.deleteRecord(record)
    .subscribe(
      succ => this.reloadList(),
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }
}

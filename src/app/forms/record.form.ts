import {
  Component,
  Output,
  Input,
  EventEmitter
} from '@angular/core';
import { Record } from '../models/record.model';

@Component({
  selector: 'record-form',
  templateUrl: './record.form.html'
})
export class RecordForm {
  @Input() record: Record;
  @Output() formSubmitted = new EventEmitter();
  @Output() formClosed = new EventEmitter();

  submit(): void{
  this.formSubmitted.emit({
      "record": this.record
    });
  }

  close(): void{
    this.formClosed.emit({});
  }

}

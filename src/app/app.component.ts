import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { AppState } from './app.service';
import { UserService } from './services/user.service';
import { User } from './models/user.model';
import { MdSnackBar } from '@angular/material';
import { LoginForm } from './forms/login.form';
import { UserForm } from './forms/user.form';
import { Router } from '@angular/router';

@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  providers: [ UserService, MdSnackBar ]
})
export class AppComponent implements OnInit {
  private hideSignUp: boolean = true;
  private hideLogIn: boolean = true;
  private loggedIn: boolean = false;
  private role: string = '';
  private userId: number = 0;
  private user: User;

  constructor(
    public appState: AppState,
    public userService: UserService,
    public mdSnackBar: MdSnackBar,
    public router: Router
  ) {
    this.user = new User(0,'','','','','','','','','','');
  }

  public ngOnInit() {
    if(localStorage.getItem('loggedIn') !== null){
      this.loggedIn = localStorage.getItem('loggedIn') == 'true';
    }
    else{
      this.loggedIn = false;
    }
    this.role = localStorage.getItem('role') || '';
    if(localStorage.getItem('userId') !== null){
      this.userId = parseInt(localStorage.getItem('userId'));
    }
    else{
      this.userId = 0;
    }
  }

  public signup(obj: any) {
    var this_ = this;
    this.userService.signUp(obj.user, obj.password)
      .subscribe(
        res => {
          this_.login({ email: obj.user.email, password: obj.password });
          this.hideSignUp = true;
        },
        err => {
          this.mdSnackBar.open(err, '', { duration: 1000 });
        }
      );
  }

  public login(obj: any) {
    this.userService.logIn(obj.email, obj.password)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token);
          this.loggedIn = true;
          localStorage.setItem('loggedIn', 'true');
          this.role = res.role;
          localStorage.setItem('role', res.role);
          this.userId = +res.id;
          localStorage.setItem('userId', '' + res.id);
          this.hideLogIn = true;
          this.mdSnackBar.open('login successful', '', { duration: 1000 });
          this.backHome();
        },
        err => {
          this.mdSnackBar.open(err, '', { duration: 1000 });
        }
      );
  }

  public logout() {
    localStorage.removeItem('token');
    this.loggedIn = false;
    this.role = '';
    this.userId = 0;
    this.mdSnackBar.open('logout successful', '', { duration: 1000 });
    this.backHome();
  }

  private backHome(){
    this.router.navigate(['/']);
  }

}

import {
  Component,
  Output,
  Input,
  EventEmitter,
  ViewChild
} from '@angular/core';
import { User } from '../models/user.model';

@Component({
  selector: 'user-form',
  templateUrl: './user.form.html'
})
export class UserForm {
  @Input() user: User;
  @Input() forCRUD: boolean;

  @Output() formSubmitted = new EventEmitter();
  @Output() formClosed = new EventEmitter();
  @Output() formDeleted = new EventEmitter();

  private insertUserPassword: string;

  submit(): void {
    this.formSubmitted.emit({
      "user": this.user,
      "password": this.insertUserPassword
    });
  }

  close(): void {
    this.formClosed.emit({});
  }

  delete(): void {
    this.formDeleted.emit({
      "user": this.user,
      "password": this.insertUserPassword
    });
  }

}

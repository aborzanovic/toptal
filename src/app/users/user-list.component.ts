import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { MdSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'user-list',
  styleUrls: [ './user-list.component.scss' ],
  templateUrl: './user-list.component.html',
  providers: [ UserService, MdSnackBar ]
})
export class UserListComponent implements OnInit {
  public users: User[] = [];
  private page: number = 1;
  private perPage: number = 5;
  private userNum: number = 0;
  private showUserInfo: boolean = false;
  private showInsertForm: boolean = false;
  private infoUser: User;
  private insertUser: User;

  constructor(
    public appState: AppState,
    public userService: UserService,
    public mdSnackBar: MdSnackBar,
    public router: Router
  ) {
    this.infoUser = new User(0,'','','','','','','','','','');
    this.insertUser = new User(0,'','','','','','','','','','');
  }

  reloadList(): void{
    this.userService.getUsersNum()
    .subscribe(
        (num: number) => this.userNum = num,
        err => {
          this.mdSnackBar.open(err, '', { duration: 1000 });
        }
    );
    this.userService.getUsers(this.page, this.perPage)
    .subscribe(
      (users: User[]) => this.users = users,
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  public ngOnInit() {
    this.reloadList();
  }

  prev(): void {
    if(this.page > 1){
      this.page -= 1;
      this.reloadList();
    }
  }

  next(): void {
    let maxPages = Math.floor(this.userNum / this.perPage);
    if(this.userNum % this.perPage > 0) maxPages++;
    if(this.page < maxPages){
      this.page += 1;
      this.reloadList();
    }
  }

  update(obj: any): void {
    this.userService.updateUser(obj.user, obj.password)
    .subscribe(
      succ => {
        this.reloadList();
        this.showUserInfo = false;
      },
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  delete(obj: any): void {
    this.userService.deleteUser(obj.user)
    .subscribe(
      succ => {
        this.reloadList();
        this.showUserInfo = false;
      },
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  new(obj: any): void {
    this.userService.insertUser(obj.user, obj.password)
    .subscribe(
      succ => {
        this.reloadList();
        this.showInsertForm = false;
      },
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  loadRecords(user: User): void {
    this.router.navigate(['./records', {userId: user.id}]);
  }

  canViewRecords(): boolean {
    let role = localStorage.getItem('role');
    return role !== null && role === 'admin';
  }
}

import { OpaqueToken } from "@angular/core";

export let APP_CONFIG = new OpaqueToken("app.config");

export interface IAppConfig {
    apiUrl: string;

    // user routes
    loginUrl: string;
    signupUrl: string;

    getUsersUrl: string;

    getModeratorsUrl: string;

    // record routes
    getRecordsUrl: string;
}

const AppConfig: IAppConfig = {
    apiUrl: "/server/api",

    loginUrl: "/session",
    signupUrl: "/user",

    getUsersUrl: "/user",

    getModeratorsUrl: "/moderator",

    getRecordsUrl: "/record"
};

// include base for dev
// AppConfig.apiUrl = 'http://dev.toptal.com' + AppConfig.apiUrl;

AppConfig.loginUrl = AppConfig.apiUrl + AppConfig.loginUrl;
AppConfig.signupUrl = AppConfig.apiUrl + AppConfig.signupUrl;
AppConfig.getUsersUrl = AppConfig.apiUrl + AppConfig.getUsersUrl;
AppConfig.getModeratorsUrl = AppConfig.apiUrl + AppConfig.getModeratorsUrl;
AppConfig.getRecordsUrl = AppConfig.apiUrl + AppConfig.getRecordsUrl;

export { AppConfig };

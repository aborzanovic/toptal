export class Record {
  constructor(
    public id: number,
    public userId: number,
    public distance: number,
    public time: number,
    public date: Date
  ){}

  public dateString(): string {
    var mm = this.date.getMonth() + 1;
    var dd = this.date.getDate();

    return [this.date.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
           ].join('-');
  }
}

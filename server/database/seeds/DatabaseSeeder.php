<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Record;
use App\Role;
use App\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // seed roles
      $admin = new Role();
      $admin->name         = 'admin';
      $admin->display_name = 'Administrator';
      $admin->description  = 'User is allowed to manage and edit other users and moderators';
      $admin->save();

      $moderator = new Role();
      $moderator->name         = 'moderator';
      $moderator->display_name = 'Moderator';
      $moderator->description  = 'User is allowed to manage and edit other users';
      $moderator->save();

      $userRole = new Role();
      $userRole->name         = 'user';
      $userRole->display_name = 'User';
      $userRole->description  = 'User is allowed to manage his own records';
      $userRole->save();

      // seed permissions
      $manageUsers = new Permission();
      $manageUsers->name         = 'manage-users';
      $manageUsers->display_name = 'Manage Users';
      $manageUsers->description  = 'crud users';
      $manageUsers->save();

      $manageModerators = new Permission();
      $manageModerators->name         = 'manage-moderators';
      $manageModerators->display_name = 'Manage Moderators';
      $manageModerators->description  = 'crud moderators';
      $manageModerators->save();

      $manageRecords = new Permission();
      $manageRecords->name         = 'manage-records';
      $manageRecords->display_name = 'Manage Records';
      $manageRecords->description  = 'crud records';
      $manageRecords->save();

      $admin->attachPermissions(array($manageUsers, $manageModerators, $manageRecords));
      $moderator->attachPermission($manageUsers);
      $userRole->attachPermission($manageRecords);

      // add a real administrator
      $user1 = new User();
      $user1->first_name = 'Big';
      $user1->last_name = 'Brother';
      $user1->password = bcrypt('password');
      $user1->address = '221b Baker Street';
      $user1->city = 'London';
      $user1->zip = '12345';
      $user1->country = 'Great Britain';
      $user1->email = 'bigbrother@gmail.com';
      $user1->phone = '123456789';
      $user1->note = '';
      $user1->avatar = '';

      $user1->save();
      $user1->attachRole($admin);

      // add a real moderator
      $user2 = new User();
      $user2->first_name = 'Little';
      $user2->last_name = 'Brother';
      $user2->password = bcrypt('password');
      $user2->address = '221b Baker Street';
      $user2->city = 'London';
      $user2->zip = '12345';
      $user2->country = 'Great Britain';
      $user2->email = 'littlebrother@gmail.com';
      $user2->phone = '123456789';
      $user2->note = '';
      $user2->avatar = '';

      $user2->save();
      $user2->attachRole($moderator);

      // seed users
      $user_num = rand(50, 100);
      factory(User::class, $user_num)->create()->each(function ($user) use ($userRole) {
        $user->attachRole($userRole);
        $user->records()->saveMany(factory(Record::class, 10)->make());
      });
    }
}

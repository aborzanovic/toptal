import {
  Component,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'login-form',
  templateUrl: './login.form.html'
})
export class LoginForm {
  private email: string;
  private password: string;

  @Output() formSubmitted = new EventEmitter();
  @Output() formClosed = new EventEmitter();

  submit(): void{
    this.formSubmitted.emit({
      "email": this.email,
      "password": this.password
    });
  }

  close(): void{
    this.formClosed.emit({});
  }

}

import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import { User } from '../models/user.model';
import { APP_CONFIG, IAppConfig } from '../app.config';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';

@Injectable()
export class UserService {

  constructor(
    @Inject(APP_CONFIG) private config: IAppConfig,
    private http: Http,
    private authHttp: AuthHttp
    ){}

  logIn(email: string, password: string): Observable<any>{
    var input = new FormData();
    input.append('email', email);
    input.append('password', password);

    return this.http.post(this.config.loginUrl, input)
                  .first()
                  .map((res: Response) => res.json())
                  .catch(this.handleError);
  }

  signUp(user: User, password: string): Observable<any>{
    return this.insertUser(user, password);
  }

  getUsersNum(): Observable<number> {
    return this.authHttp.get(this.config.getUsersUrl + '?page=1&per_page=1')
                    .first()
                    .map((res: Response) => res.json().total)
                    .catch(this.handleError);
  }

  getUsers(page: number = 1, perPage: number = 20): Observable<User[]> {
    return this.authHttp.get(this.config.getUsersUrl + '?page=' + page + '&per_page=' + perPage)
                    .first()
                    .map((res: Response) => this.extractArray(res.json().users))
                    .catch(this.handleError);
  }

  updateUser(user: User, password: string): Observable<any> {
    var input = new FormData();
    input.append('first_name', user.firstName);
    input.append('last_name', user.lastName);
    input.append('address', user.address);
    input.append('city', user.city);
    input.append('zip', user.zip);
    input.append('country', user.country);
    input.append('email', user.email);
    input.append('password', password);
    input.append('phone', user.phone);
    input.append('note', user.note);

    return this.authHttp.post(this.config.getUsersUrl + '/' + user.id, input)
                  .first()
                  .catch(this.handleError);
  }

  deleteUser(user: User): Observable<any> {
    return this.authHttp.delete(this.config.getUsersUrl + '/' + user.id)
                  .first()
                  .catch(this.handleError);
  }

  insertUser(user: User, password: string): Observable<any> {
    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/x-www-form-urlencoded');

    var input = new URLSearchParams();
    input.set('first_name', user.firstName);
    input.set('last_name', user.lastName);
    input.set('address', user.address);
    input.set('city', user.city);
    input.set('zip', user.zip);
    input.set('country', user.country);
    input.set('email', user.email);
    input.set('password', password);
    input.set('phone', user.phone);
    input.set('note', user.note);

    return this.http.put(this.config.getUsersUrl, input, { headers: myHeader })
                  .first()
                  .catch(this.handleError);
  }

  getModeratorsNum(): Observable<number> {
    return this.authHttp.get(this.config.getModeratorsUrl + '?page=1&per_page=1')
                    .first()
                    .map((res: Response) => res.json().total)
                    .catch(this.handleError);
  }

  getModerators(page: number = 1, perPage: number = 20): Observable<User[]> {
    return this.authHttp.get(this.config.getModeratorsUrl + '?page=' + page + '&per_page=' + perPage)
                    .first()
                    .map((res: Response) => this.extractArray(res.json().moderators))
                    .catch(this.handleError);
  }

  updateModerator(user: User, password: string): Observable<any> {
    var input = new FormData();
    input.append('first_name', user.firstName);
    input.append('last_name', user.lastName);
    input.append('address', user.address);
    input.append('city', user.city);
    input.append('zip', user.zip);
    input.append('country', user.country);
    input.append('email', user.email);
    input.append('password', password);
    input.append('phone', user.phone);
    input.append('note', user.note);

    return this.authHttp.post(this.config.getModeratorsUrl + '/' + user.id, input)
                  .first()
                  .catch(this.handleError);
  }

  deleteModerator(user: User): Observable<any> {
    return this.authHttp.delete(this.config.getModeratorsUrl + '/' + user.id)
                  .first()
                  .catch(this.handleError);
  }

  insertModerator(user: User, password: string): Observable<any> {
    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/x-www-form-urlencoded');

    var input = new FormData();
    input.append('first_name', user.firstName);
    input.append('last_name', user.lastName);
    input.append('address', user.address);
    input.append('city', user.city);
    input.append('zip', user.zip);
    input.append('country', user.country);
    input.append('email', user.email);
    input.append('password', password);
    input.append('phone', user.phone);
    input.append('note', user.note);

    return this.authHttp.put(this.config.getModeratorsUrl, input, { headers: myHeader })
                  .first()
                  .catch(this.handleError);
  }

  private extract(json: any): User {
    let user = new User(
      json.id,
      json.email,
      json.first_name,
      json.last_name,
      json.address,
      json.city,
      json.zip,
      json.country,
      json.phone,
      json.note,
      json.avatar
    );
    return user;
  }

  private extractArray(arr: any[]): User[] {
    let users: User[] = [];
    var this_ = this;
    arr.forEach(function(json: any){
      users.push(this_.extract(json));
    });

    return users;
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = err;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw('Error: ' + errMsg);
  }
}

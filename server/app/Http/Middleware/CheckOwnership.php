<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Response;

class CheckOwnership
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::parseToken()->toUser();
        $notOwner = true;
        if($user && $request->id != null){
          $notOwner = !$user->records()->find($request->id);
        }
        if($user && $request->input('user_id') != null){
          $notOwner = intval($user->id) !== intval($request->input('user_id'));
        }

        if(!$user || (!$user->hasRole('admin') && $notOwner)){
          return response(['error' => 'you do not own this'], Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}

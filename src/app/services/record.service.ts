import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import { Record } from '../models/record.model';
import { APP_CONFIG, IAppConfig } from '../app.config';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';

@Injectable()
export class RecordService {

  constructor(
    @Inject(APP_CONFIG) private config: IAppConfig,
    private http: Http,
    private authHttp: AuthHttp
    ){}

  getRecordsNum(userId: number): Observable<number> {
    return this.authHttp.get(this.config.getRecordsUrl + '?user_id=' + userId + '&page=1&per_page=1')
          .first()
          .map((res: Response) => res.json().total)
          .catch(this.handleError);
    }

  getRecords(userId: number, page: number = 1, perPage: number = 20): Observable<Record[]> {
    return this.authHttp.get(this.config.getRecordsUrl + '?user_id=' + userId + '&page=' + page + '&per_page=' + perPage)
          .first()
          .map((res: Response) => this.extractArray(res.json().records))
          .catch(this.handleError);
  }

  insertRecord(record: Record): Observable<any>{
    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/x-www-form-urlencoded');

    var input = new URLSearchParams();
    input.set('distance', '' + record.distance);
    input.set('time', '' + record.time);
    input.set('date', record.dateString());
    input.set('user_id', '' + record.userId);

    return this.authHttp.put(this.config.getRecordsUrl, input, { headers: myHeader })
          .first()
          .catch(this.handleError);
  }

  updateRecord(record: Record): Observable<any>{
    var input = new FormData();
    input.append('distance', record.distance);
    input.append('time', record.time);
    input.append('date', record.dateString());

    return this.authHttp.post(this.config.getRecordsUrl + '/' + record.id, input)
          .first()
          .catch(this.handleError);
  }

  deleteRecord(record: Record): Observable<any>{
    return this.authHttp.delete(this.config.getRecordsUrl + '/' + record.id)
          .first()
          .catch(this.handleError);
  }

  private extract(json: any): Record {
    let record = new Record(
      json.id,
      json.userId,
      json.distance,
      json.time,
      new Date(json.date)
    );
    return record;
  }

  private extractArray(arr: any[]): Record[] {
    let records: Record[] = [];
    var this_ = this;
    arr.forEach(function(json: any){
      records.push(this_.extract(json));
    });

    return records;
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = err;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw('Error: ' + errMsg);
  }
}

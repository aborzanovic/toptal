<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use Exception;
use Illuminate\Http\Request;
use \App\User;
use \App\Record;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class RecordController extends Controller
{
  protected function getRecordNum(Request $request){
    $userId = $request->input('user_id');
    $num = User::find($userId)->records()->count();
    return $num;
  }

  public function getRecords(Request $request){
    $page = intval($request->input('page', 1));
    $perPage = intval($request->input('per_page', 0));
    $userId = $request->input('user_id');
    $records = User::find($userId)->records()->take($perPage)->skip(($page - 1) * $perPage)->orderBy('date', 'desc')->get();
    $total = $this->getRecordNum($request);
    return compact('records', 'total');
  }

  public function getRecord(Request $request, $id){
    $record = Record::find($id);
    return $record;
  }

  private function populateRecord(Record $record, Request $request){
    $validator = Validator::make($request->all(), [
      'date' => 'bail|date_format:Y-m-d|before:tomorrow',
      'time' => 'bail|required|integer|min:0',
      'distance' => 'required|integer|min:0',
    ]);

    if ($validator->fails()) {
      foreach($validator->errors()->all() as $error){
        throw new Exception($error);
      }
    }

    $record->date = $request->input('date');
    $record->time = $request->input('time');
    $record->distance = $request->input('distance');

    return $record;
  }

  public function insert(Request $request){
    $record = new Record;
    $user = JWTAuth::parseToken()->toUser();

    if($request->input('user_id')){
      if($user->hasRole('admin') || $user->id === intval($request->input('user_id'))){
        $record->user_id = $request->input('user_id');
      }
      else{
        return response(['error' => 'you\'re not admin'], Response::HTTP_UNAUTHORIZED);
      }
    }
    else{
      $record->user_id = $user->id;
    }

    try{
      $this->populateRecord($record, $request);
      $record->save();
    }
    catch (Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function update(Request $request, $id){
    $record = Record::find($id);

    if(!$record){
      return response(['error' => 'record doesn\'t exists'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    try{
      $this->populateRecord($record, $request);
      $record->save();
    }
    catch (Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function delete(Request $request, $id){
    $record = Record::find($id);

    if(!$record){
      return response(['error' => 'record doesn\'t exists'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    try{
      $record->delete();
    }
    catch (Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// session

Route::group(['prefix' => 'session'], function () {

  Route::post('/', 'UserController@login'); // login

});

// user

Route::group(['prefix' => 'user'], function () {

  Route::put('/', 'UserController@insertUser'); // signup and insert

  Route::group(['middleware' => ['jwt.auth', 'permission:manage-users']], function () {

      Route::get('/', 'UserController@getUsers');
      Route::get('{id}', 'UserController@getUser');
      Route::post('{id}', 'UserController@updateUser');
      Route::delete('{id}', 'UserController@deleteUser');

  });

});

// moderator

Route::group(['prefix' => 'moderator',
  'middleware' => ['jwt.auth', 'permission:manage-moderators']], function () {

  Route::get('/', 'UserController@getModerators');
  Route::get('{id}', 'UserController@getModerator');
  Route::put('/', 'UserController@insertModerator');
  Route::post('{id}', 'UserController@updateModerator');
  Route::delete('{id}', 'UserController@deleteModerator');


});

// record

Route::group(['prefix' => 'record',
'middleware' => ['jwt.auth','permission:manage-records']], function () {

  Route::put('/', 'RecordController@insert');

  Route::group(['middleware' => ['ownership']], function () {

    Route::get('/', 'RecordController@getRecords');
    Route::get('{id}', 'RecordController@getRecord');
    Route::post('{id}', 'RecordController@update');
    Route::delete('{id}', 'RecordController@delete');

  });

});

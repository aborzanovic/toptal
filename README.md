Setup:

  -recommended Apache virtual server (dev.toptal.com)

  Database:
    -required MySQL server
    -required database named 'toptal'
    -required user with crud privileges for the database (usr: 'toptal', pwd: 'toptal')
    -(all of these parameters are changable in '/server/.env' and '/server/config/database.php' files)

  Backend API:
    -run the following commands:
      'cd .../toptal/server'
      'php composer.phar install'
      'php artisan migrate'
      'php artisan db:seed'

  Frontend SPA:
    -run the following commands:
      'npm install'
      'npm run build'

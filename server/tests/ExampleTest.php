<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use \App\User;
use \App\Record;


class ExampleTest extends TestCase
{
    protected $id = 0;

    public function testSignUp()
    {
      $this->json('PUT', '/server/api/user',
      [ 'email' => 'test@gmail.com',
        'password' => 'password',
        'first_name' => 'test',
        'last_name' => 'test',
        'address' => 'address',
        'city' => 'city',
        'zip' => 'zip',
        'country' => 'country',
        'phone' => '123456789',
        'note' => 'test note'
      ])->assertResponseOk();
    }

    public function testLogin()
    {
      $this->json('POST', '/server/api/session',
      [ 'email' => 'test@gmail.com',
        'password' => 'password'
      ])->seeJsonStructure([
        'token', 'role', 'id'
      ]);
    }

    public function testAddRecord()
    {
      $user = User::first();
      $lastUser = User::orderBy('id', 'desc')->first();
      $this->json('PUT', '/server/api/record',
      [ 'date' => '2017-01-01',
        'time' => '100',
        'distance' => '100',
        'user_id' => $lastUser->id
      ],
      $this->headers($user))->assertResponseOk();
    }

    public function testReadRecord()
    {
      $user = User::first();
      $lastRecord = Record::orderBy('id', 'desc')->first();
      $this->json('GET', '/server/api/record/' . $lastRecord->id,
      [],
      $this->headers($user))->seeJsonStructure([
        'date', 'distance', 'time', 'user_id', 'id'
      ]);
    }

    public function testDelete()
    {
      $user = User::first();
      $lastUser = User::orderBy('id', 'desc')->first();
      $this->json('DELETE', '/server/api/user/' . $lastUser->id,
      [],
      $this->headers($user))
         ->assertResponseOk();
    }

    protected function headers($user = null)
    {
        $headers = ['Accept' => 'application/json'];

        if (!is_null($user)) {
            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer '.$token;
        }

        return $headers;
    }
}

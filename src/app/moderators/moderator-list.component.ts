import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'moderator-list',
  styleUrls: [ './moderator-list.component.scss' ],
  templateUrl: './moderator-list.component.html',
  providers: [ UserService, MdSnackBar ]
})
export class ModeratorListComponent implements OnInit {
  public moderators: User[] = [];
  private page: number = 1;
  private perPage: number = 5;
  private userNum: number = 0;
  private showModeratorInfo: boolean = false;
  private showInsertForm: boolean = false;
  private infoModerator: User;
  private insertModerator: User;

  constructor(
    public appState: AppState,
    public userService: UserService,
    public mdSnackBar: MdSnackBar
  ) {
    this.infoModerator = new User(0,'','','','','','','','','','');
    this.insertModerator = new User(0,'','','','','','','','','','');
  }

  reloadList(): void{
    this.userService.getModeratorsNum()
    .subscribe(
        (num: number) => this.userNum = num,
        err => {
          this.mdSnackBar.open(err, '', { duration: 1000 });
        }
    );
    this.userService.getModerators(this.page, this.perPage)
    .subscribe(
      (moderators: User[]) => this.moderators = moderators,
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  public ngOnInit() {
    this.reloadList();
  }

  prev(): void {
    if(this.page > 1){
      this.page -= 1;
      this.reloadList();
    }
  }

  next(): void {
    let maxPages = Math.floor(this.userNum / this.perPage);
    if(this.userNum % this.perPage > 0) maxPages++;
    if(this.page < maxPages){
      this.page += 1;
      this.reloadList();
    }
  }

  update(obj: any): void {
    this.userService.updateModerator(obj.user, obj.password)
    .subscribe(
      succ => {
        this.reloadList();
        this.showModeratorInfo = false;
      },
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  delete(obj: any): void {
    this.userService.deleteModerator(obj.user)
    .subscribe(
      succ => {
        this.reloadList();
        this.showModeratorInfo = false;
      },
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }

  new(obj: any): void {
    this.userService.insertModerator(obj.user, obj.password)
    .subscribe(
      succ => {
        this.reloadList();
        this.showInsertForm = false;
      },
      err => {
        this.mdSnackBar.open(err, '', { duration: 1000 });
      }
    );
  }
}

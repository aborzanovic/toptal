export class User {
  constructor(
    public id: number,
    public email: string,
    public firstName: string,
    public lastName: string,
    public address: string,
    public city: string,
    public zip: string,
    public country: string,
    public phone: string,
    public note: string,
    public avatar: string
  ){}
}
